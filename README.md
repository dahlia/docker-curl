docker-curl
===========

Nothing more than a `curl`-installed [`alpine`][1] image.

Public domain.

[1]: https://hub.docker.com/_/alpine/
